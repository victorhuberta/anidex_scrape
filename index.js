var fs = require('fs');
var express = require('express');
var request = require('request');
var cheerio = require('cheerio');
var cluster = require('cluster');
var app = express();
// var Clarifai = require('clarifai');
var animal_dictionary = {};

var wstream = fs.createWriteStream('./animal_database.json');

wstream.on('finish', function () {
	console.log('Write stream finished');
	wstream.end();
});

app.get('/scrape', function (req, res) {
  var url = 'http://a-z-animals.com/animals/';
  request(url, function (error, response, html) {
  	if (!error) {
  		var $ = cheerio.load(html);
  		$('.article_az').filter(function () {
  			console.log('.article_az');
			  $(this).children('tr').each(function (i, elem) {
  				console.log('tr');
  				$(this).children('td').each(function (i, elem) {
  					console.log('td');
  					$(this).children('ul').each(function (i, elem) {
  						console.log('ul');
  						$(this).children('li').each(function (i, elem) {
	  						console.log('li');
	  						getAnimalInfo($(this).children('a').text(), $(this).children('a').attr('href'));
	  					});
  					});
  				});
  			});
  		});
  	} else {
      console.log(error);
    }
  });
});

function getAnimalInfo(name, postfix) {
	var url = 'http://a-z-animals.com' + postfix;
	request(url, function (error, response, html) {
		if (!error) {
			var $ = cheerio.load(html);
			animal_dictionary[name] = {};
			$('.article_facts').filter(function () {
				$(this).children('tr').each(function (i, elem) {
					var data = $(this);
					var b = data.children().first().children().first();
					if (b.children().first().text() === '') {
            var key = b.text();
            var value = data.children('td').last().text();
            if (key !== '' && value !== '') {
              animal_dictionary[name][key] = value; 
            }
					} else {
            var key = b.children().first().text();
            var value = data.children('td').last().text();
            if (key !== '' && value !== '') {
              animal_dictionary[name][key] = value; 
            }
					}

					console.log(animal_dictionary);
					if (Object.keys(animal_dictionary).length >= 591) {
            console.log('WRITING TO FILE...');
            wstream.write(JSON.stringify(animal_dictionary));
          }
				});
			});
		} else {
      console.log(error);
    }
	});
}

app.listen('8081');
console.log('Magic happens on port 8081');

module.exports = app;

// client = new Clarifai({
// 	id: 'kkz_Hxnp8bS5JctolcJnd3O3PYEFH9lo2wfTPCPd',
// 	secret: 'ZZH7QcRcRrqxChLQuSMO8oHnpFwAWtyruHhPaYAb'
// });

// var buffer = fs.readFileSync('/Users/victorhuberta/Desktop/PICTURES/test_image-2.jpg');

// client.tagFromBuffers('image', buffer, function (err, results) {
// 	console.log(results);
// });